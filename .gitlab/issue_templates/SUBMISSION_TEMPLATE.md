A simple outline of a submission could be (and this may change):

```
Describe your idea.

Which of the four categories for submissions does this address?

What kind of backing would this kind of project need in addition to what's already being provided?

What are open questions you need help with?

Planned Architecture (if you know this)

Data requirements

Short description of the benefits
```