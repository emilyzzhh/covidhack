# COVID-19 Virtual Hackathon

AWS, Fairwinds, Gitlab, and Datadog have teamed up to provide an end-to-end infrastructure solution to enable an application that addresses the pandemic.

This will start with a pitch competition. Using this GitLab project we will accept proposals for projects already underway, or new ideas for projects. Any proposed solution needs to help address at least one of the following:

- Reduce the Spread of the Virus
- Find a Cure for the Virus
- Educate the Public on Covid-19
- Rebuild the Economy

Representatives from each of the backing companies will judge and vote on a solution. The winning project will be announced April 15th. Application development will start immediately after.

* **AWS** will be offering 6 months of application hosting support*
* **GitLab** will be where the project lives
* **Datadog** will be offering free monitoring for 6 months*
* **Fairwinds** will be offering free Kubernetes managed services for 6 months 

We believe with this combination of partners we can provide full infrastructure support to get an idea off the ground, scaling, and moving. Our goal is to address the situation in a practical way, and give our engineering teams and the community a way to be involved.

## Submission

To submit an idea for the pitch competition submit your idea as an issue to this repo and tag your issue with "submission" - open repo, open issue, open converstaion. When you create an issue choose the "Submission Template" template to get you started.

## Deadline

**This project will be open to pitches until April 10th.**

## Judging

Judges from each company bring technical expertise and have backgrounds in things like microbiology, physics, and healthcare (this list is subject to change):

* Brandon Jung - VP of Alliances at GitLab
* Ilan Rabinovitch - VP of Product and Community at Datadog
* Sarah Zelechoski - VP of Engineering at Fairwinds
* EJ Etherinton - CTO at Fairwinds
* Oxana K. Pickeral, Ph.D., MBA | Global Segment Leader - Healthcare & Life Sciences | AWS Partner Network - from Amazon Web Services
* Bob Wise - General Manager at AWS

## Slack Team
**[Join the Slack team here.](https://join.slack.com/t/covidhack-workspace/shared_invite/zt-cyqsij2a-cbpHlRo8~k6GavDiiRtYuQ)**

## Fine Print
*All parties involved are interested in helping out in whatever way they can. However, due to the fact that a project has not yet been accepted, and there are many variables, these free offerings may require some limits to be feasible.

This is the initial announcement. More details coming soon. Merge requests are welcome.