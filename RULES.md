# Rules

## Purpose
The purpose of this document is to set forth the right and expectations of those submitting ideas for consideration.

1. *Be Kind*
    * Have a look at our [Code of Conduct](CODE_OF_CONDUCT.md) for exactly what that means.
2. *Be Open*
    * This project is open source, the communication and conversation should be open as well. The selected project will be considered a free donation to this collaborative group and will be licensed under Apache 2.0
3. *Be Considerate*
    * It is our express intent that any submissions that are not selected remain the IP of the submitter. In the case where there is more than one similiar idea, as deemed by the judges, the project chosen will be the first one of the tuple submitted. Projects that are not chosen shall remain the intellectual property of the submittor.
